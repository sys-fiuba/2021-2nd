### A Pluto.jl notebook ###
# v0.16.0

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : missing
        el
    end
end

# ╔═╡ 725e1e44-1595-11ec-0365-0b245b8c8761
begin
	import Pkg; Pkg.activate()
	using SyS
end

# ╔═╡ fe7915ce-85cd-4eea-87dd-878fcea31d9c
let
	ns = -10:20
	
	x(n) = u(n) - u(n - 6)
	
	xs = x.(ns)
	
	stem(ns, xs; title="asht", xlabel="n")
end

# ╔═╡ 4d04ec69-e80e-4f04-b8ef-93b5983915be
md"""
La frecuencia es:

$(@bind frec Slider(0.2:0.1:5; show_value=true))
"""

# ╔═╡ ec1b2cca-28d3-4851-9a2c-58abc65526ae
md"""
Me gusta que el período me dio $(1/frec).
"""

# ╔═╡ 4d6d5b8f-4c42-4be8-9450-c44f5702524f
plot(t -> sin(2π*frec*t), 0, 4)

# ╔═╡ c5dd81da-1b05-4d4e-afb0-4b6c53697ac8
xd1(n) = u(n) - u(n - 6)

# ╔═╡ 861ba683-2d83-4bed-a763-1186394481be
range(0; stop=4, length=7)   |> collect

# ╔═╡ d717d07b-4f79-4199-a004-9c1ab5ef6826
xd2(n) = xd1(n) * n

# ╔═╡ a5f9e0fc-fadf-45a0-a193-c5a39a9c8c2c
let
	ns = -5:10
	
	stem(ns, xd1.(ns))
	stem!(ns, xd2.(ns))
end

# ╔═╡ 5ec75aba-9035-4c8f-a2d5-fd743f2d07e7
begin
	x1vec = xd1.(0:10)#[ 1, 1, 1, 1, 1, 1 ]
	x2vec = xd2.(0:8)
end

# ╔═╡ af2f67c1-3c25-4559-bbaa-fad52cd137fd
y = conv(x1vec, x2vec)

# ╔═╡ d5961447-2cbf-40f1-881f-459bf438574d
let
	ns = range(0; length=length(y))
	stem(ns, y)
end

# ╔═╡ 04507bff-250a-44ce-9632-3f276cbd280e
length(x1vec), length(x2vec), length(y)

# ╔═╡ Cell order:
# ╠═725e1e44-1595-11ec-0365-0b245b8c8761
# ╠═fe7915ce-85cd-4eea-87dd-878fcea31d9c
# ╟─4d04ec69-e80e-4f04-b8ef-93b5983915be
# ╟─ec1b2cca-28d3-4851-9a2c-58abc65526ae
# ╠═4d6d5b8f-4c42-4be8-9450-c44f5702524f
# ╠═c5dd81da-1b05-4d4e-afb0-4b6c53697ac8
# ╠═a5f9e0fc-fadf-45a0-a193-c5a39a9c8c2c
# ╠═5ec75aba-9035-4c8f-a2d5-fd743f2d07e7
# ╠═af2f67c1-3c25-4559-bbaa-fad52cd137fd
# ╠═04507bff-250a-44ce-9632-3f276cbd280e
# ╠═d5961447-2cbf-40f1-881f-459bf438574d
# ╠═861ba683-2d83-4bed-a763-1186394481be
# ╠═d717d07b-4f79-4199-a004-9c1ab5ef6826
